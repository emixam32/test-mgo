import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Movie {
  nom: string;
  realisateur: string;
  nationnalite: string;
  nb_diffusion: number;
  nb_premiere_partie: number;
  ratio: number;
  moyenne_diffusion_par_an: number;
}

export interface Country {
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) {
  }

  getMovies(): Observable<[Movie]> {
    return this.http.get<[Movie]>('http://127.0.0.1:8001/movie');
  }
}
