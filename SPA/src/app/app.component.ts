import {Component, OnInit, ViewChild} from '@angular/core';
import {Country, Movie, MovieService} from './service/movie.service';
import {ColumnMode, DatatableComponent} from '@swimlane/ngx-datatable';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  movies: Movie[];
  statistiques: any;
  countries: Country[];
  selectedCountries = [];
  columns: object[];
  topColumns;
  rows: object[];
  columnMode = ColumnMode.force;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  filter: { realisateur: string, nom: string, countries: Array<string> };

  constructor(private movieService: MovieService, private modalService: NgbModal) {
    this.countries = [];
    this.selectedCountries = [];
    this.statistiques = {mostShow: [], bestRatio: [], director: []};
    this.filter = {realisateur: '', nom: '', countries: []};

  }

  ngOnInit(): void {
    this.movieService.getMovies().subscribe((movies: Movie[]) => {
      this.movies = this.rows = movies;
      this.countries = this.getCountries();
      this.filter.countries = [...this.countries.map(country => country.name)];
      this.getStats(this.movies);
    });

    this.topColumns = {
      director: [
        {prop: 'name', name: 'Réalisateur'},
        {prop: 'ratio', name: 'Ratio'},
      ],
      mostShow: [
        {prop: 'nom', name: 'Nom du film'},
        {prop: 'nb_diffusion', name: 'Nombre de diffusion'},
      ],
      bestRatio: [
        {prop: 'nom', name: 'Nom du film'},
        {prop: 'ratio', name: 'ratio'},
      ]
    };

    this.columns = [
      {prop: 'nom', name: 'Nom du film'},
      {prop: 'realisateur', name: 'Réalisateur'},
      {prop: 'annee_protuction', name: 'Année de production'},
      {prop: 'nationnalite', name: 'Nationnalité'},
      {prop: 'derniere_diffusion', name: 'Dernière diffusion'}];
  }

  updateFilter(event, key: string): void {
    if (key !== 'countries') {
      this.filter[key] = event.target.value.toLowerCase();
    }

    // Filter data
    this.rows = this.movies.filter((d) => {
      return (d.nom.toLowerCase().indexOf(this.filter.nom) !== -1 || !this.filter.nom) &&
        (d.realisateur.toLowerCase().indexOf(this.filter.realisateur) !== -1 || !this.filter.realisateur) &&
        (this.filter.countries.includes(d.nationnalite));
    });
    // Back to first page
    this.table.offset = 0;
  }


  open(content): void {
    this.modalService.open(content);
  }

  getCountries(): Country[] {
    let index = 1;

    return [...new Set(this.movies.map(movie => movie.nationnalite))].map(country => {
      return {id: index++, name: country};
    });
  }

  // TODO explode function
  getStats(movies: Movie[]): any {
    // Most show
    this.statistiques.mostShow = movies.sort((a, b) => {
      return a.nb_diffusion - b.nb_diffusion;
    }).slice(0, 5);

    this.movies.forEach(movie => {
      movie.ratio = movie.nb_premiere_partie / movie.nb_diffusion;
    });

    // Best Ratio
    this.statistiques.bestRatio = movies.sort((a, b) => {
      return a.ratio - b.ratio;
    }).slice(0, 5);


    // Best director ratio
    const directorList = this.arrayGroupBy(this.movies, 'realisateur');

    for (const [key, value] of Object.entries(directorList)) {
      let ratio = 0;
      directorList[key].forEach((movie: Movie) => ratio += movie.moyenne_diffusion_par_an);
      // TODO define directorList type
      // @ts-ignore
      this.statistiques.director.push({name: key, ratio: ratio / value.length});
    }
    this.statistiques.director = this.statistiques.director.slice(0, 5);
  }

  arrayGroupBy(arrayToReduce, key): any {
    return arrayToReduce.reduce((final, current) => {
      (final[current[key]] = final[current[key]] || []).push(current);
      return final;
    }, {});
  }
}
