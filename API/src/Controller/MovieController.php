<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    /**
     * @Route("/movie", name="movie")
     */
    public function index()
    {
        $path    = dirname(dirname(dirname(__FILE__))) . '/resources/movies.json';
        $content = file_get_contents($path);
        $json    = json_decode($content, true);

        return $this->json($json);
    }
}
